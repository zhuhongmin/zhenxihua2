package com.yu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZhuHongMingController {
    @GetMapping("/zhuhongming")
    public String name(){
        return "朱红明";
    }
}
