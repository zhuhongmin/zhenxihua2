package com.yu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZhuHongMinController {
    @GetMapping("/zhuhongmin")
    public String name(){
        return "朱宏民";
    }
}
